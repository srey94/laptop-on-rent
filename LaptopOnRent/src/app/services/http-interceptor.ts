/*
Authors : Sreyaas R.,Srikrishna Sasidharan,Hari A
*/
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpResponse } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { HttpEvent } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
//import { SpinnerService } from './SpinnerService';
import {} from '../services/authentication-service';
 
@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
 
     constructor() { }
 
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
 
        //this.spinnerService.show();
        

        req = req.clone({
            setHeaders: {
              Authorization: 'Bearer '+sessionStorage.getItem('jwt')
            }
          });
        return next
            .handle(req)
            // .pipe(
            //   finalize(() => { 
            //     //this.spinnerService.hide();  
            //     })
            // );
    }
}