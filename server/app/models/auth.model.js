/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/

var sql = require('../../db.js');

var User = function (user) {
    this.email = user.email;
    this.pswd = user.pswd;
    this.uId = user.uId;
}

User.getUserbyEmail= function (email,result){
    sql.query("Select email, pswd, uId  from authentication where email = ? ", email, function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            result(null, res);

        }
    });
} 



module.exports = User;