/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormArrayName, FormGroupDirective, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/authentication-service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})


export class UserProfileComponent implements OnInit {
  profileForm: FormGroup;
  constructor(private route: Router, private authService: AuthService, private _snackBar: MatSnackBar) { }
  firstNameCC;
  lname;
  username;
  password;
  address;
  address1; inputState;
  city; zip;
  save = false;
  edit = true;
  onInit;


  ngOnInit() {
    this.onInit = false;
    this.profileForm = new FormGroup({
      'firstname': new FormControl({ value: '22576767', disabled: true }, { validators: [Validators.required, Validators.minLength(2), Validators.maxLength(30)] }),
      'lastname': new FormControl({ value: '22576767', disabled: true }, { validators: [Validators.required, , Validators.minLength(2), Validators.maxLength(30)] }),
      'username': new FormControl({ value: '', disabled: true }, { validators: [Validators.required] }),
      'password': new FormControl({ value: '', disabled: true }, { validators: [Validators.required, Validators.minLength(6), Validators.maxLength(20)] }),
      'address': new FormControl({ value: '22576767', disabled: true }, { validators: [Validators.required] }),
      'address2': new FormControl({ value: '22576767', disabled: true }),
      'phoneNumber': new FormControl({ value: '22576767', disabled: true }, { validators: [Validators.required] }),
      'city': new FormControl({ value: '22576767', disabled: true }, { validators: [Validators.required] }),
      'state': new FormControl({ value: '22576767', disabled: true }, { validators: [Validators.required] }),
      'zip': new FormControl({ value: '22576767', disabled: true }, { validators: [Validators.required] })
    });
    this.profileForm.get('state').setValue('NS')

    var uid = sessionStorage.getItem('uid');
    let body = {
      'uid': uid
    }


    this.authService.fetchProfileDetails(body).subscribe(data => {


      if (data.success) {
        data = data.data;


        this.profileForm.get('firstname').setValue(data[0].firstName);
        this.profileForm.get('lastname').setValue(data[0].lastName);
        this.profileForm.get('username').setValue(data[0].email);
        this.profileForm.get('address').setValue(data[0].addressLine1);
        this.profileForm.get('address2').setValue(data[0].addressLine2);
        this.profileForm.get('city').setValue(data[0].city);
        this.profileForm.get('state').setValue(data[0].state);
        this.profileForm.get('zip').setValue(data[0].postcode);
        this.profileForm.get('phoneNumber').setValue(data[0].phoneNumber);
      }
      else {


        this._snackBar.open(data.message, "Error Occurred Please try later", {
          duration: 1500,
        });
      }
    })
  }

  onSubmit() {
    this.save = true;
    this.edit = false;

    this.profileForm.get('firstname').enable();
    this.profileForm.get('lastname').enable();
    this.profileForm.get('address').enable();
    this.profileForm.get('address2').enable();
    this.profileForm.get('city').enable();
    this.profileForm.get('state').enable();
    this.profileForm.get('zip').enable();
    this.profileForm.get('phoneNumber').enable();

  }

  onSave() {
    var uid = sessionStorage.getItem('uid');


    let body = {
      'uId': uid,
      'firstName': this.profileForm.get('firstname').value,
      'lastName': this.profileForm.get('lastname').value,
      'addressLine1': this.profileForm.get('address').value,
      'addressLine2': this.profileForm.get('address2').value,
      'phoneNumber': this.profileForm.get('phoneNumber').value,
      'city': this.profileForm.get('city').value,
      'state': this.profileForm.get('state').value,
      'postcode': this.profileForm.get('zip').value
    }

    this.authService.editProfileDb(body).subscribe(data => {


      if (data.success) {
        data = data.data;
        this.profileForm.get('firstname').setValue(data.firstName);
        this.profileForm.get('lastname').setValue(data.lastName);
        this.profileForm.get('address').setValue(data.addressLine1);
        this.profileForm.get('address2').setValue(data.addressLine2);
        this.profileForm.get('city').setValue(data.city);
        this.profileForm.get('state').setValue(data.state);
        this.profileForm.get('zip').setValue(data.postcode);
        this.profileForm.get('phoneNumber').setValue(data.phoneNumber);

        this._snackBar.open("Update Success", "OK", {
          duration: 1500,
        });
      }
      else {


        this._snackBar.open(data.message, "Error Occurred Please try later", {
          duration: 1500,
        });
      }
    })
    this.save = false;
    this.edit = true;
    this.profileForm.disable();
  }

}
