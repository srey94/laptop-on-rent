/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/

var sql = require('../../db.js');
var CryptoJS = require("crypto-js");

var User = function (registerUser) {

    this.uId = registerUser.uId;
    this.firstName = registerUser.firstName;
    this.lastName = registerUser.lastName;
    this.email = registerUser.email;
    this.password = registerUser.password;
    this.addressLine1 = registerUser.addressLine1;
    this.addressLine2 = registerUser.addressLine2;
    this.city = registerUser.city;
    this.state = registerUser.state;
    this.postcode = registerUser.postcode;
    this.phoneNumber = registerUser.phoneNumber;
    this.isAdmin = registerUser.isAdmin;
    this.isLocked = registerUser.isLocked;

}

User.addUser = function (registerUser, result) {

    sql.query("Select max(uId) as c from user ", function (err, res) {

        if (err) {

            result(err, null);
        }


        else {

            //result(null, res);
            sql.query("select email as existing_user from user where email = ? ", registerUser.email, function (err, resu) {

                if (err) {

                    result(err, null);
                }
                else if (resu[0] && registerUser.email == resu[0].existing_user) {

                    result(null, null);

                }
                else {


                    registerUser.uID = res[0].c + 1;
                    var str = "insert into user(uId,firstName,lastName,email,phoneNumber,isAdmin,isLocked";
                    var str2 = "('" + registerUser.uID + "', '" + registerUser.firstName + "', '" + registerUser.lastName + "', '" + registerUser.email + "','" + registerUser.phoneNumber + "'," + 0 + "," + 0;

                    if (registerUser.addressLine1 != undefined && registerUser.addressLine1 != "") {
                        str += ",addressLine1";
                        str2 += ",'" + registerUser.addressLine1 + "'";
                    } else {
                        str += ",addressLine1";
                        str2 += ",''";
                    }

                    if (registerUser.addressLine2 != undefined && registerUser.addressLine2 != "") {
                        str += ",addressLine2";
                        str2 += ",'" + registerUser.addressLine2 + "'";
                    } else {
                        str += ",addressLine2";
                        str2 += ",''";
                    }
                    if (registerUser.city != undefined && registerUser.city != "") {
                        str += ",city";
                        str2 += ",'" + registerUser.city + "'";
                    } else {
                        str += ",city";
                        str2 += ",''";
                    }
                    if (registerUser.state != undefined && registerUser.state != "") {
                        str += ",state";
                        str2 += ",'" + registerUser.state + "'";
                    } else {
                        str += ",state";
                        str2 += ",''";
                    }
                    if (registerUser.postcode != undefined && registerUser.postcode != "") {
                        str += ",postcode";
                        str2 += ",'" + registerUser.postcode + "'";
                    } else {
                        str += ",postcode";
                        str2 += ",''";
                    }

                    str += ")"
                    str2 += ")"

                    var q = str + " values " + str2;


                    var q1 = q;


                    sql.query(q1, function (error, response) {
                        if (error) {

                            result(error, null);
                        } else {
                            //result(null,response);

                            var ciphertext = CryptoJS.AES.encrypt(registerUser.password, 'cipher1234').toString();


                            var q2 = "insert into authentication(uId,email,pswd) VALUES('" + registerUser.uID + "','" + registerUser.email + "','" + ciphertext + "')";
                            sql.query(q2, function (e, r) {
                                if (e) {

                                    result(e, null);
                                } else {

                                }
                            })


                        }
                    });
                    result(null, res);

                }
            });
        }


    });



}
module.exports = User;
