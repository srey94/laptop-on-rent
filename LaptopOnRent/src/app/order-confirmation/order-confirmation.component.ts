/*
  Authors: Srikrishna Sasidharan
*/
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { CartService } from '../services/cart-service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AddToCartDialog } from '../cart/cart.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.css']
})
export class OrderConfirmationComponent implements OnInit {

  orderItems = []
  @Output() cartItem : EventEmitter<any>  = new EventEmitter();
  totalPrice=0;
  price=0;
  tax=0;
  securityPrice = 100.00
  datesche ;
  promotions;
  constructor(public dialog: MatDialog,private cartService: CartService,private _snackBar: MatSnackBar,private _route:Router) { }
  addrObj; payObj;
  orderId;

  ngOnInit() {
    this.orderId = sessionStorage.getItem('orderid')
    this.datesche = sessionStorage.getItem('scheduledDelivery')
    this.addrObj = JSON.parse(sessionStorage.getItem('selectedAddressFull'))
    this.payObj = sessionStorage.getItem('paymentFull')
    for (var key in sessionStorage) {
      if (key.indexOf('OrderItems') !== -1){
        var element = JSON.parse(sessionStorage.getItem(key))
        this.orderItems.push(JSON.parse(sessionStorage.getItem(key)))
        this.price += ( parseInt(element['perDay']) * parseInt(element['quantity']) )
        sessionStorage.removeItem(key)
      }
    }
    var uid = sessionStorage.getItem('uid')
    if(this.orderItems.length>=1){
      this.orderItems.forEach(element => {
        
        this.cartService.removeCartItem({cartid:element['cartid'], uid:uid}).subscribe(data=>{
        if(data.success){
            
            this.cartService.getCartCount({'uid':uid}).subscribe(e=>{
              if(e.success){
                this.cartItem.emit(e.count);
              }else{
                this.cartItem.emit('0');
              }
            });
          }else{
            this._snackBar.open(data.message, "ok", {
              duration: 2000,
            });
          }
        });

      });

      this.tax = ( (this.price*15)/100)
      this.totalPrice = this.price + this.tax + this.securityPrice
    }
    this.cartService.getPromotions().subscribe(data=>{
      this.promotions = data.promotions
    }   );


  }

// future work
  addToCartFromExplore(prod: any){

  
  
   
      const dialogRef = this.dialog.open(AddToCartDialog, {
        width: 'auto',
        height: 'auto',
     
      data: { prodID: prod.productID, Product: prod.Product,
        Company:prod.Company,  perDay: prod.perDay, productImg: prod.productImg,
        maxQuantity: prod.availabilty
      }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        this._route.navigate(["cart"])
      });
    

  }

}
