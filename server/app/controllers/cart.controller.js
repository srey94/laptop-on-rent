/*
    Author: Srikrishna Sasidharan
*/

let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');


var CartModel = require('../models/cart.model')

// adds product to user cart.
exports.addToCart = (req,res)=>{
    class Cart{
        
        constructor(){
            this.uid = req.body.uid;
            this.productID = req.body.productID;
            this.quantity = req.body.quantity;
            this.cartId = req.body.cartId;
            this.softwares = req.body.softwares;
            this.fromDate = req.body.fromDate;
            this.toDate = req.body.toDate;
            this.maxQuantity = req.body.maxQuantity;
        }
    }
    
    cart = new Cart();

    CartModel.addToCartForUser1(cart, function(err, result){
        if(result){
           if(cart.softwares!=undefined){ 
    CartModel.addToCartForUser(cart, function (err, task){
        if(task){
            
            res.json({
                success: true,
                message: 'Product inserted to cart.'
              });
        }else {
            res.json({
              success: false,
              message: 'Error occured while adding product to cart'
            });
          }
       

    });
}
        }
    })
   
}

//get all cart items based on user
 exports.getCartDetailsForUser = (req,res) =>{

    class Cart{
        constructor(){
            this.uid = req.body.uid;
            this.productID = req.body.productID;
            this.quantity = req.body.quantity;
            this.cartId = req.body.cartId;
            this.softwares = req.body.softwares;
            this.fromDate = req.body.fromDate;
            this.toDate = req.body.toDate;
        }
    }
    cart = new Cart();

    CartModel.getCartDetailsForUser(cart, function (err, task){
        
        if(task){
            var cartItems=[]
            for (var i = 0; i < task.length; i++){
                cartItems[i] = task[i]
            }
            res.json({
                success: true,
                cartItems: cartItems
              });
        }else {
            res.json({
              success: false,
              message: 'Error occured while retrieving cart details.'
            });
          }
       

    });
 }

// remove user cart item
 exports.removeCartItem = (req,res) =>{
     
    class Cart{
        constructor(){
            this.uid = req.body.uid;
            this.productID = req.body.productID;
            this.quantity = req.body.quantity;
            this.cartId = req.body.cartid;
            this.softwares = req.body.softwares;
            this.fromDate = req.body.fromDate;
            this.toDate = req.body.toDate;
        }
    }

    cart = new Cart();
    CartModel.removeCartItem(cart, function(err,task){
        if(task){
            res.json({
                success: true,
                message: "Item removed from cart."
              });
        }else{
            res.json({
                success: false,
                message: "Error occured when removing cart item."
              });
        }
    })

 }

 // update cart item for user
 exports.updateCartItem = (req,res) =>{
    class Cart{
        constructor(){
            this.uid = req.body.uid;
            this.productID = req.body.productID;
            this.quantity = req.body.quantity;
            this.cartId = req.body.cartid;
            this.softwares = req.body.softwares;
            this.fromDate = req.body.fromDate;
            this.toDate = req.body.toDate;
        }
    }

    cart = new Cart();
    CartModel.updateCartItem(cart, function(err,task){
        if(task){
            res.json({
                success: true,
                message: "Cart Item updated"
            })
        }else{
            res.json({
                success: false,
                message: "Error occured when updating cart item."
            })
        }
    })
 }

 // update software required for the user
 exports.updateSoftware = (req,res)=>{
    class Cart{
        constructor(){
            this.uid = req.body.uid;
            this.productID = req.body.productID;
            this.quantity = req.body.quantity;
            this.cartId = req.body.cartid;
            this.softwares = req.body.softwares;
            this.fromDate = req.body.fromDate;
            this.toDate = req.body.toDate;
        }
    }

    cart = new Cart();
    CartModel.updateSoftware(cart, function(err,task){
        if(task){
            res.json({
                success: true,
                message: "Cart Item updated"
            })
        }else{
            res.json({
                success: false,
                message: "Error occured when updating cart item."
            })
        }
    })
 }

 //get user cart count
 exports.getCartCount = (req,res) => {
    class Cart{
        constructor(){
            this.uid = req.body.uid;
            this.productID = req.body.productID;
            this.quantity = req.body.quantity;
            this.cartId = req.body.cartid;
            this.softwares = req.body.softwares;
            this.fromDate = req.body.fromDate;
            this.toDate = req.body.toDate;
        }
    }

    cart = new Cart();
    CartModel.getCartCount(cart, function(err,task){
        if(task){
            res.json({
                success: true,
                count: task[0].c,
                message: "Cart Item updated"
            })
        }else{
            res.json({
                success: false,
                count:0,
                message: "Error occured when updating cart item."
            })
        }
    })
 }

 //get promotional products for explore more options
 exports.getPromotions = (req,res)=>{
    
    

    CartModel.getRandomPromotions( function(err,task){
        if(task){
            var promotions=[]
            for (var i = 0; i < task.length; i++){
                promotions[i] = task[i]
            }
            res.json({
                success: true,
                promotions: promotions
            })
            
        }
    });
   
        
    
    
 }