module.exports = (app) => {
    const check = require('../controllers/check.controller.js');
    let loginHnadler = require('../controllers/login.controller.js');
    let cartHandler = require('../controllers/cart.controller.js');
    let registerHandler = require('../controllers/registerUser.controller');

    let productHandler = require('../controllers/product.controller.js');
    let landingHandler = require('../controllers/landing.controller.js');
    let addressHandler = require('../controllers/address.controller.js');
    let paymentHandler = require('../controllers/payment.controller.js');

    let reviewHandler = require('../controllers/review.controller.js');

    let emailHandler = require('../controllers/email.controller');
    let updatePassowordHandler = require('../controllers/updatePassword.controller');
    let orderHandler = require('../controllers/order.controller');

    let fetchProfileDetailsHandler = require('../controllers/getProfileDetails.controller');
    let editProfileDbHandler = require('../controllers/editProfileDb.controller');

    let pastOrdersHandler = require('../controllers/allOrders.controller');

    // Create a new Note
    app.post('/check', check.create);

    // Retrieve all check
    app.get('/check', check.findAll);

    // Retrieve a single Note with noteId
    app.get('/check/:noteId', check.findOne);

    // Update a Note with noteId
    app.put('/check/:noteId', check.update);

    // Delete a Note with noteId
    app.delete('/check/:noteId', check.delete);


    app.post('/login', loginHnadler.login);

    app.post('/login', loginHnadler.login);

    app.get('/', loginHnadler.checkToken, loginHnadler.index);

    app.get('/', loginHnadler.checkToken, loginHnadler.index);

    //cart
    app.post('/addToUserCart', cartHandler.addToCart);
    app.post('/getCartDetailsForUser', cartHandler.getCartDetailsForUser);
    app.post('/removeCartItem', cartHandler.removeCartItem);
    app.post('/updateCartItem', cartHandler.updateCartItem);
    app.post('/updateSoftware', cartHandler.updateSoftware);    
    app.post('/getCartCount', cartHandler.getCartCount);    
    app.get('/getPromotions',cartHandler.getPromotions);
    //cart
    

    //address
    app.post('/addAddress', addressHandler.addAddress);
    app.post('/getAddressForUser', addressHandler.getAddressForUser)
    app.post('/removeAddress', addressHandler.removeAddress)
    app.post('/editAddress', addressHandler.editAddress)
    //address

    //payment
    app.post('/addNewPayment', paymentHandler.addPayment)
    app.post('/getUserPaymentMethods',paymentHandler.getPaymentForUser)
    //payment

    //review
    app.post('/submitReview',reviewHandler.addReview);
    app.get('/getReview/:id',reviewHandler.getReview);

    //register user

    app.post('/registerUser', registerHandler.registerUser);

    //reset password
    app.post("/updatePwd",updatePassowordHandler.updatePassword);

    //fetch profile details

    app.post("/fetchProfile",fetchProfileDetailsHandler.fetchProfile);

    //edit profile db

    app.post("/editProfileDb",editProfileDbHandler.editProfileDb);

    //email service
    app.post('/send-email',emailHandler.sendEmail);

    //product routes
    app.get('/product/:id',productHandler.product);
    
    app.get('/quantity/:id',productHandler.quantity);
    
    //search routes
    app.post('/productSearch',productHandler.search);
    app.post('/filter', productHandler.filter)
    app.post('/productId',productHandler.getProductID);
    //landing routes
    app.get('/landing/',landingHandler.landing)
    //admin routes
    app.post('/addProduct',productHandler.addProduct)
    app.get('/getUsers',fetchProfileDetailsHandler.adminProfile)
    app.post('/lockUser',editProfileDbHandler.lockUser)
    //order
    app.post('/placeOrder', orderHandler.placeOrder)

    app.post('/allOrders',pastOrdersHandler.allOrders);
    //order
}
