/*
  Author: Hari Arunachalam
*/
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AdminService } from '../services/admin.services';
import { Router} from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'app-add-laptop',
  templateUrl: './add-laptop.component.html',
  styleUrls: ['./add-laptop.component.css']
})
export class AddLaptopComponent implements OnInit {

  isLinear = false;
  laptopForm: FormGroup;
  techForm: FormGroup;
  offerForm: FormGroup;
  constructor(private snackBar: MatSnackBar,private adminService:AdminService,public dialog: MatDialog,public route:Router) { }

  ngOnInit() {
    this.laptopForm = new FormGroup({
      Company: new FormControl(''),
      Product: new FormControl(''),
      TypeName: new FormControl(''),
      Weight: new FormControl(''),
      productImg: new FormControl(''),
      Price: new FormControl(''),
     
      
    });

    this.techForm = new FormGroup({
      Ram: new FormControl(''),
      Memory: new FormControl(''),
      Inches: new FormControl(''),
      Cpu: new FormControl(''),
      Gpu: new FormControl(''),
      ScreenResolution: new FormControl(''),
      OpSys: new FormControl(''),
      
    });

    this.offerForm = new FormGroup({
      
      discount: new FormControl(''),
      fromDate: new FormControl(''),
      toDate: new FormControl(''),
    
    });
  }

  // Add a laptop
  addLaptop(){
     let body ={
      'laptopForm':this.laptopForm.value,
      'techForm':this.techForm.value
    }

    this.adminService.addProduct(body).subscribe(data=>{
      const dialogRef = this.dialog.open(DialogOverviewExample, {
        width: '250px',
        data: data
      });
  
      dialogRef.afterClosed().subscribe(result => {
        this.route.navigate(["/", "adminHome"]);
      });
    })
  }
}



@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog.html',
})
export class DialogOverviewExample {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExample>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}