/*
    Author: Thanigaiselvan senthil shanmugam
*/

let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');
const mongoose = require('mongoose');
const ReviewModel = require('../models/review.model')

//Insert review to the Mongo Database
exports.addReview = (req, res) => {

    const review = new ReviewModel({
        _id: new mongoose.Types.ObjectId(), // generates new unique Id for each record
        uid: req.body.uid,
        productID: req.body.productID,
        userName: req.body.userName,
        star: req.body.star,
        review: req.body.review
    });
    //saving the data in database and logging it
    review.save().then(result=>{
       
    })
    .catch(err=>console.log(err));
    res.status(201).json({
        success: true,
        message:"Review Submitted"
    });
};

//Fetch reviews for the product from the database
exports.getReview = (req, res) => {

    let productId = req.params.id;
    const GetReview = new ReviewModel();
    
    ReviewModel.find({'productID':productId}).then(result=>{
        res.status(200).json({
            success: true,
            message: 'Review Fetched',
            reviews: result
        });
        })
    .catch(err=>console.log(err));
    

};
