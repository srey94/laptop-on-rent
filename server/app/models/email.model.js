/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/

var sql = require('../../db.js');
var path = require('path');
var nodeMailer = require('nodemailer');
var bodyParser = require('body-parser');
var CryptoJS = require("crypto-js");

var User = function (user) {
    this.email = user.email;
    this.otp = user.otp;
}

User.sendEmail = function (email, otp, res) {
    let transporter = nodeMailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'laptoponrentg01@gmail.com',
            pass: 'Laptop@rent'
        }

    });
    let mailOptions = {
        from: '"LaptopOnRent" <laptoponrentg01@gmail.com>', // sender address
        to: email, // list of receivers
        subject: 'Password Reset', // Subject line
        html: "Please use the generated password <b>" + otp + "</b> to reset your Password ", // html body
    };


    sql.query("Select uId,email, pswd from authentication where email = ? ", email, function (err, result) {
        if (err) {
            console.log("error: ", err);
            res(err, null);
        }
        else {



            userPassword = result[0].pswd;


            var bytes = CryptoJS.AES.decrypt(userPassword, 'cipher1234');
            var originalPassword = bytes.toString(CryptoJS.enc.Utf8);




            var ciphertext = CryptoJS.AES.encrypt(otp, 'cipher1234').toString();


            var q2 = "update authentication set pswd = '" + ciphertext + "' where email = '" + email + "'";
            sql.query(q2, function (e, r) {
                if (e) {
                    console.log("error :", e);
                    res(e, null);
                } else {
                    console.log("User OTP inserted in Auth Table")
                }
            })
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                else {

                    console.log('Message %s sent: %s', info.messageId, info.response);
                    res(null, result);
                    return console.log('Email Sent' + info.response);


                }

            });
            //res(null, res);

        }
    });

}
module.exports = User;