# Project - LaptopOnRent

* *Date Created*: 01 FEB 2020
* *Last Modification Date*: 04 APR 2020

## Authors

** [Senthil Shanmugam,Thanigaiselvan](th876217@dal.ca); [HARI ARUNACHALAM](hariarun.19@gmail.com); [Srikrishna Sasidharan](sr950863@dal.ca); [Sreyas Naaraayanan Ramanathan](srey94@gmail.com);**

## Getting Started

Url to website: http://laptoponrent-env.eba-nrvrbmir.us-east-1.elasticbeanstalk.com/homepage

Admin account:
email:hari.arunachalam@dalca
password: password
1. This application is currently not supported in IE browser, Ipad Pro (Not responsive due to the size in between medium and large screen).
2. Refreshing the page does not work with parameters in certain pages.
3. Currently, the application is not supported in Internet Explorer and the application is not responsive enough only on ipad pro

## Features Developed: 

## Overall project completion percentage : 95%


**Senthil Shanmugam,Thanigaiselvan(th876217@dal.ca)**

1. Landing Page
2. Product page
3. Product Review

Landing page (Completed):

The front end logic and files are available in the folder named landing-page which includes HTML, CSS and typescript files

Name of the files in the folder:

1.landing-page.component.css
2.landing-page.component.html
3.landing-page.component.ts

The Service call to the backend is available in the folder named services

Name of the files in the folder:

1.landingService.ts

The backend logic for the landing page features are available under the folders controllers, models

1. landing.controller.js
2. landing.model.js 

API:

http://54.162.42.117:3000/landing/

Functionality:

1. Landing page retrieves the laptops to be displayed in the home page of laptop on rent application.
On creation a service call to the landingService.ts is called which request a get call to the express.Js
The retrieved data will be classified based on the response and will be dynamically updated in the front end
based on the category. Three function calls will be performed to load the data in to the landing page.

2. Promotional banners will be displayed in the home page which redirects and generate results based on the 
banner selection.

Apart from this the navigation bar menu will be displayed based on the user roles.


Product page (Completed):

The front end logic and files are available in the folder named product-page which includes HTML, CSS and typescript files

Name of the files in the folder:

1.product-page.component.css
2.product-page.component.html
3.product-page.component.ts

The Service call to the backend is available in the folder named services

Name of the files in the folder:

1.productService.ts

The backend logic for the landing page features are available under the folders controllers, models

1. product.controller.js
2. product.model.js

API:

http://54.162.42.117:3000/product/:id
http://54.162.42.117:3000/quantity/:id
http://54.162.42.117:3000/getReview/:id 

Functionality:

1. Product page retrieves the data of a particular product that is called from the landing page or search result page.
A four backend call will be requested to build the product page. 
     1. Get call to retrieve product details.
	 2. Get call to retrieve product available date.
	 3. Get call to retrieve quantity avaialable. The maximum quantity that can be order will be 3
	    even though there are products available in the inventory.
	 4. Get call to retrieve for the customer reviews for the particular product.
2. Calculating the average customer review for the product after the product reviews are retrieved.
3, Logic to validate the fromDate and toDate to make sure that the user orders the product based on availability.
4. Form validation the quantity.
5. Displaying the technical details of the product in the  table format.
6. Displaying the user reviews and limiting the visible review to 2 and adding a functionality to view all the reviews.

Product Review (Completed):

The front end logic and files are available in the folder named product-page/review which includes HTML typescript files

Name of the files in the folder:

1.review.dialog.html
2.review.dialog.ts

The Service call to the backend is available in the folder named services

Name of the files in the folder:

1.reviewService.ts

The backend logic for the landing page features are available under the folders controllers, models

1. review.controller.js
2. review.model.js

http://54.162.42.117:3000/submitReview

Functionality:

1. This creates  a new dialog which allows user to add review for the particular product. Before adding the review
for the product the user is expected to login to the application. On successfull completion of review, a post call
will be created.


**HARI ARUNACHALAM(hari.arunachalam@dal.ca)**


1.Search Feature ( Completed fully)
- Asynchronous Autosuggestion with both brand or model name
- Search resulting based on search text
- Advanced search with mulitple filters 
- Asynchronous pagination

2.Administrator:(80% completed)
-Administrator home (completed)
-Add laptop with proper form validations (completed)
-Edit Laptops(Not completed)
-Lock/unlock users(Completed)

#files

Angular components
------------------------
add-laptop 	- Admin Feature  (Complete)
admin-home 	- Admin Feature  (Complete)
admin-user 	- Admin Feature  (Complete)
login      	- Admin Feature  (blocking locked user and session-storage management)
search-result   - Search Feature (Complete)
user-navigation - Search Feature (Autosuggestion, Admin conditions)
       bar

Angular Services
--------------------
admin-services    - Admin Feature (Complete)
http-interceptor  - JWT management
search-services   - Search Feature (Complete)

Express js Controller
-----------------------
Login controller         - Admin Feauture(Admin login, jwt management)
Product controller       - Admin and Search features(Add laptop,autosuggestion, search with filter)
editProfileDb controller - Admin Feature (lock/unlock users)

Express js Models
--------------------
getProfileDetails - Admin Feature (Get users)
editProfileDb     - Admin Feature (lock/unlock users)
product           - Search Features (auto suggestion, search with filters)


**Srikrishna Sasidharan(sr950863@dal.ca)**



Main URL: http://laptoponrent-env.eba-nrvrbmir.us-east-1.elasticbeanstalk.com/homepage
Bluenose URL:  https://web.cs.dal.ca/~tshanmugam/group1-master/laptopOnRent/homepage

Feature 1 (cart): completed 100%
--------------------------------
1. Add products to cart (Product page and search page):
	User can add products to cart from product page and search product page. User have to choose rental start date, end date, quantity and optional softwares.
	Cart count is updated based on user selection.	
	Files - Angular components /product-page/product-page.component(.css/.html/.ts) and  /search-result/search-result.component(.css/.html/.ts)
			Express.js- /server/routes/check.route.js , /server/controllers/cart.controller.js, /server/models/cart.model.js

2 Cart Page: 
	Cart feature has two different flows one for the guest user and another for the logged In user.
	All methods in cart feature is handled to support both users. SessionStorage (web storage) is used for guest user when the user adds some product to cart before logging in.
	Micro services are called in asynchronous manner to update user information to the user cart record.
	Additionally, both users can add promotional items from the cart page to their cart. 
	When the user clicks checkout cart button, they are nagivated to login page if guest user. Once logged in where user can login or register. 
	Once logged in all the cart items stored in session storage are pushed to the database and the user is nagivated to choose address for delivery.
 	
	Files -  Angular components /cart/cart.component(.css/.html/.ts)
			Express.js- /server/routes/check.route.js , /server/controllers/cart.controller.js, /server/models/cart.model.js
			
Feature 2 (Order Placement): completed 100%
---------------------------------------------

1 Address page: 
	When user clicks checkout button from cart, user will be redirected to choose delivery address page. Here user can select an existing address for delivery or edit the existing address or remove existing address.
	User can also add new address using add new address dialog box. The address added will be saved to the user record.
	User has to select an delivery address before proceeding to payment.
	
	Files -  Angular components /choose-address/choose-address.component(.css/.html/.ts)
			Express.js- /server/routes/check.route.js , /server/controllers/address.controller.js, /server/models/address.model.js
	
2 Payment page:
	When user clicks proceed in address page, user will be redirected to choose payment method page. Here user can choose an existing payment method or pay with new card which will be stored to the user record.
	User has to select a payment method if not paying with new card and place order.
	
	Files -  Angular components /choose-payment/choose-payment.component(.css/.html/.ts)
			Express.js- /server/routes/check.route.js , /server/controllers/payment.controller.js, /server/models/payment.model.js
	
	
3 Order confirmation page:
	After clicking place order from choose payment page. User address selection, payment selection is added to user order records and user will be redirected to order confirmation page.
	Here user can view the current placed order details, selected address and payment method details.
	
	Files -  Angular components /order-confirmation/order-confirmation.component(.css/.html/.ts)
			Express.js- /server/routes/check.route.js , /server/controllers/order.controller.js, /server/models/order.model.js
	



**Sreyas Naaraayanan Ramanathan(srey94@dal.ca)**

Feature 1 : User Profile 
(Fully Complete)

* Login Page.
* Registration Page.
* Edit Profile.
* Reset Password( EMail sent to user for resetting password)

Feature 2 - Orders
(90% complete)

* Orders Page.
* Current Orders and Past Orders

The landing page is the home page.One can navigate to Login Page.
After log-in,once the user is logged in the ,on the dropdown menu they get the Orders,User Profile and Logout  

From the profile button on the nav-bar, Orders Page can be accessed. 
 
Orders page has two features namely Current Orders and Past Orders.
In Current Orders, the users can extend the order and provide the date and save.(Order extension feature alone is yet to be finished)
In past orders one can write a review to the product and view their past orders

In registration page ,one can register .After registration they are routed to the landing page.

See deployment for notes on how to deploy the project on a live system.

## Files Used 

* Angular Components(HTML,and CSS files)
regsiter  - User Profile Management Feature
login	  - User Profile Management Feature
orders	  - Orders Feature
reset-password - Send E-mail for reset password
user-profile	- View and Edit User Profile Details
update-password-db -User Profile Management Feature

* Angular Service
-----------------
authentication-service - Complete services related to User Profile Management
payment-service - Service related to orders

* Express Controller
-------------------
allOrders.controller.js  - Orders Controller
getProfileDetails.controller - Fetch User Profile
email.controller.js   - Send E-mail
editProfileDb.controller.js - Edit user profile
login.controller.js - Login Controller
registerUser.controller.js - Register Controller
updatePassword.controller.js - Update password controller

* Model
-------------------
allOrders.model.js - Orders Model
auth.model.js  -Login  Model
editProfileDb.model.js -  Edit profile
email.model.js - Email Model
getProfileDetails.model.js - Fecth Profile Details Model
registerUser.model.js - Registration Model
updatePassword - Update Password Model


## Prerequisites & Installation

 
Can directly access the site from given url.


To run locally,

Prerequisites:
Node js
Express js installed
Angular installed.

Clone the repository from git lab.

Express server
Navigate to server folder
Open cmd prompt/terminal
type in npm install
then type node app.js
Then access from localhost:3000(default port)

Angular server
Navigate to the LaptopOnRent folder
Open cmd prompt/terminal
type in npm install
then type ng serve.


Then access from localhost:4200(default port)

Also need to change service to call from localhost:3000


## Built With

1. [Angular 8] (https://angular.io/) The web framework used
2. [Angular Material] https://material.angular.io/ - To build aesthetic components 
3. [Bootstrap 4] (https://getbootstrap.com/) - Used to build responsive application
4. [Express Js](https://expressjs.com/) - Backend restful services

## Sources Used

Bootstrap 4 Documentation: https://getbootstrap.com/docs/4.1/getting-started/introduction/
Angular Materiial IO Documentation: https://material.angular.io/
Angular 8 Documentation: https://angular.io/docs
Advanced topics in web tutorials - For express js 
https://medium.com/wdstack/bootstrap-4-form-examples-c18ac5e9cd30
https://www.npmjs.com/package/crypto-js
https://appdividend.com/2017/08/11/send-email-in-node-js/
https://medium.com/dev-bits/a-guide-for-adding-jwt-token-based-authentication-to-your-single-page-nodejs-applications-c403f7cf04f4
https://jwt.io/introduction/


Used source code from the below article to establish communication from child component to parent component.
source : https://medium.com/@sujeeshdl/angular-parent-to-child-and-child-to-parent-communication-from-router-outlet-868b39d1ca89


Modification : The given article has only established a communication between two components. I have tried to implement it and extend its functionality to pass values from child to parent component.
### File Name
1. user-navigation-bar.component.html

*Line No 113*

```
<router-outlet (activate)="onActivate($event)"></router-outlet>


```


2. 	user-navigation-bar.component.ts

*Line No 60*

```
console.log(componentReference)
    //Below will subscribe to the cartItem emitter
    // Will receive the data from child here 

    if (componentReference.cartItem) {

      componentReference.cartItem.subscribe((data) => {
        console.log('cartItem' + data);
        localStorage.setItem('currentUser', "true");

        localStorage.getItem('currentUser');
        if (localStorage.getItem('currentUser') === "true") {
          this.userStatus = true;
        }
      });
    }

    componentReference.cartItem.subscribe((data) => {
      console.log(data);
      this.cartCount = localStorage.getItem('cartCount');
    });
```
The code above was created by adapting the code in [sujeeshdl](https://medium.com/@sujeeshdl) as shown below: 

```
@Output() searchItem: EventEmitter<any> = new EventEmitter();





onActivate(componentReference) {
   console.log(componentReference)
   componentReference.anyFunction();
}
```






### Productpage.Component.html

*Lines 227 - 228

Since the material design did not have the predefined tag to generate the User 
rating field, the following component was used.
```
<mat-star-rating [rating]="rating" [starCount]="starCount" [color]="starColor"  
(ratingUpdated)="onRatingChanged($event)"></mat-star-rating>

```

The code above was created by adapting the code in [BioPhoton]
(https://github.com/BioPhoton/angular-star-rating) as shown below: 

The author have created a new tag just like other material tags  and i have used 
this in my project to create review form with the user ratings.

*Images used in the website are retrieved from (https://www.amazon.in/)



### dialog-overview-example-dialog.html

Lines 8 - 21
---------------

```
<h3 mat-dialog-title>Select date to extend</h3>
<div mat-dialog-content>
    <mat-form-field>
        <input matInput [matDatepicker]="picker" id="date" placeholder="Choose a end date">
        <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
        <mat-datepicker #picker startView="year" [startAt]="startDate"></mat-datepicker>
    </mat-form-field>

    <div class="row">
        <button type="button" class="btn btn-dark btn-sm " id="savebtn" style="margin-left: 38%;"
            (click)="saveData()">Save</button>
    </div>

</div>
```

The code above was created by adapting the code in 
https://material.angular.io/components/dialog/overview) as shown below: 

```
<h1 mat-dialog-title>Hi {{data.name}}</h1>
<div mat-dialog-content>
  <p>What's your favorite animal?</p>
  <mat-form-field>
    <input matInput [(ngModel)]="data.animal">
  </mat-form-field>
</div>
<div mat-dialog-actions>
  <button mat-button (click)="onNoClick()">No Thanks</button>
  <button mat-button [mat-dialog-close]="data.animal" cdkFocusInitial>Ok</button>
</div>


```

- <!---How---> The code in https://material.angular.io/components/dialog/overview was implemented in angular themes
- <!---Why---> (https://material.angular.io/components/dialog/overview )'s code was used to render a date picker inside a modal for exteding order functionality
- <!---How---> Date picker was added to <mat-dialog-content> inside a mat-form-field. Buttons was added in the <mat-dialog-action>


## Acknowledgments

* Famous e-commerce websites such as Amazon,Flipkart were a source of inspiration



