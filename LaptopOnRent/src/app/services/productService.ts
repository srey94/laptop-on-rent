/*
Authors : Thanigaiselvan Senthil Shanmugham 
*/
import { HttpHeaders, HttpClient ,HttpErrorResponse} from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  
  export class ProductService{
    apiUrl: string = 'http://54.162.42.117:3000';
    headers = new HttpHeaders().set('Content-Type', 'application/json');
  
    constructor(private http: HttpClient) { }

    //addtocart
    product(data) : Observable<any>{
        let API_URL = this.apiUrl+'/product/'+data;
        return this.http.get(API_URL,data)
        .pipe(
          catchError(this.error)
        )
    }

    //quantity

    quantity(data) : Observable<any>{
      let API_URL = this.apiUrl+'/quantity/'+data;
      return this.http.get(API_URL,data)
      .pipe(
        catchError(this.error)
      )
  }



    // Handle Errors 
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

  
  }