/*
  Authors: Srikrishna Sasidharan
*/
import {  OnInit, Component, Output,EventEmitter, Inject, Input } from '@angular/core';
import {FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatDatepickerInputEvent, MatSelectChange, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CartService } from '../services/cart-service';
import { DialogData } from '../product-page/review/review.dialog';
import { element } from 'protractor';
import { ProductService } from '../services/productService';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
  
})


export class CartComponent implements OnInit {

  @Output() cartItem: EventEmitter<any> = new EventEmitter();

  quanityList: Quantity[] =[
    {value: 1, viewValue: 1},
    {value: 2, viewValue: 2},
    {value: 3, viewValue: 3}
  ];

  cartItems: Array<JSON>;
  startDate = new Date();
  endDate = new Date();
  startDateModel;
  endDateModel;
  toppings = new FormControl();
  toppingList: string[] = ['Office 365', 'Adobe Photoshop', 'Visual Studio', 'Android Studio', 'FIFA 19', 'Call of Duty'];
  showValueP1: boolean = true;
  showValueP2: boolean = true;
  quantities = new FormControl();
  cartTotalPrice: number =0;
  
  cartVal: boolean = false;
  promotions:[]

  newCartProduct: boolean = false;
  constructor(public dialog: MatDialog,private _router: Router,private cartService: CartService,private _snackBar: MatSnackBar, private productService: ProductService) { }
  q;
  quan: number = 1;
  date;

  ngOnInit() {
    this.cartTotalPrice = 0
    this.cartItems= [];

    let user = sessionStorage.getItem('uid')
    if(user == "null" || user == undefined){
      var cartCount = 0
      for (var key in sessionStorage) {
        if (key.indexOf('cartItem') !== -1){
          var obj = JSON.parse(sessionStorage.getItem(key));
          this.cartItems.push(obj);
          cartCount+=1
          
        }
      }
      
      localStorage.setItem('cartCount',''+cartCount);
      this.cartItem.emit(cartCount);
      this.cartItems.forEach(data=>{
        this.cartTotalPrice += (parseInt(data['perDay'])*parseInt(data['quantity']))
      });
      if(this.cartTotalPrice>0){
        this.cartVal = true;
      }else{
        this.cartVal = false;
      }
     
    }else{
      this.cartService.fetchCartItemsForUser({"uid":user}).subscribe(data=>{
        
      if(data.success){
        
        var cartCount = 0
        
    
        for(var i=0;i<data.cartItems.length;i++){
          cartCount+=1
          
            
          var co = parseInt(data.cartItems[i].maxQuantity)
          data.cartItems[i]['maxQuantity'] = []
            var k=0
            for(var j=1;j<=co;j++){
              data.cartItems[i]['maxQuantity'].push(j);
              k++;
            }
       
          

        }
        
         
       

        this.cartItem.emit(cartCount)
        if(this.cartTotalPrice>0){
          this.cartVal = true;
        }else{
          this.cartVal = false;
        }
        this.cartItems = data.cartItems;
       
      }else{
        this._snackBar.open(data.message, "ok", {
          duration: 2000,
        });
        

      }
      var cartCount = 0
      var dates = ""
        this.cartItems.forEach(data=>{
          cartCount+=1
        var d = data['fromDate'].indexOf('T')
        var dd = data['fromDate'].slice(0,d)

        dates += dd +" "
        this.cartTotalPrice += (parseInt(data['perDay'])* parseInt(data['quantity']) )
      });
      sessionStorage.setItem('scheduledDelivery',dates)
      sessionStorage.setItem('price',this.cartTotalPrice.toString())
      this.cartItem.emit(cartCount)
      if(this.cartTotalPrice>0){
        this.cartVal = true;
      }else{
        this.cartVal = false;
      }
      });
    }  
    this.cartService.getPromotions().subscribe(data=>{
      this.promotions = data.promotions
    }   );
  }
  
  //delete product from cart
  deleteProduct(cartid:string,productID : any){
    
    var uid = sessionStorage.getItem('uid')
    sessionStorage.removeItem('OrderItems'+productID);
    if(cartid == null || cartid == undefined){
      sessionStorage.removeItem('cartItem'+productID);
      

      
      var cartCount =+localStorage.getItem('cartCount');
      cartCount = cartCount - 1;
      this.cartItem.emit(cartCount)
      localStorage.setItem('cartCount',''+cartCount);
      this.ngOnInit();
    }else{

        this.cartService.removeCartItem({cartid:cartid, uid:uid}).subscribe(data=>{
        if(data.success){
            
            this.cartService.getCartCount({'uid':uid}).subscribe(e=>{
              if(e.success){
                this.cartItem.emit(e.count);
              }else{
                this.cartItem.emit(0);
              }
            });
            this.ngOnInit();
          }else{
            this._snackBar.open(data.message, "ok", {
              duration: 2000,
            });
          }
        });
      
  }
  } 
  
// addToCart from explore moew
  addToCartFromExplore(prod: any){

    
  
   
      const dialogRef = this.dialog.open(AddToCartDialog, {
        width: 'auto',
        height: 'auto',
     
      data: { prodID: prod.productID, Product: prod.Product,
        Company:prod.Company,  perDay: prod.perDay, productImg: prod.productImg,
        maxQuantity: prod.availabilty
      }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        this.ngOnInit();
       // this.animal = result;
      });
    

  }

  //check out cart items
  checkOutCart(){
    var uid = sessionStorage.getItem('uid')
    if (uid == 'null' || uid == undefined){
      this._router.navigate(['login']);
    }else{
      this.cartItems.forEach(element=>{
        sessionStorage.setItem('OrderItems'+element['productID'],JSON.stringify(element));
      }
      )
      
      this._router.navigate(['/choose-address']);

  }
  }


//quantity update
  onQuantityChange(event: MatSelectChange, cartId: string){
    var uid = sessionStorage.getItem('uid')
    if(uid!='null' && uid != undefined){
    this.cartService.updateCart({uid: uid, cartid:cartId,quantity:this.quantities.value}).subscribe(data =>{
      

      if(!data.success){
        this._snackBar.open(data.message, "ok", {
          duration: 2000,
        });
      } 
    });
  }

  }

  //navigates to product page
  goToProd(prom: any){
    this._router.navigate(["","product",prom.productID])
  }

  //updates software selections
  onSoftwareChange(event: MatSelectChange, cartId: string){
    let c = this.toppings.value + '';
      let  softwares = c.split(",");
      if(this.toppings.value == undefined ){
        softwares[0] = "Default"
      }
      var uid = sessionStorage.getItem('uid')
      if(uid!='null' && uid != undefined){
    this.cartService.updateSoftware({uid: uid, cartid:1,softwares:softwares}).subscribe(data =>{
      if(!data.message){
      this._snackBar.open(data.message, "ok", {
        duration: 2000,
      });
    }
    

     
    });
  }
  }
  // updates user selection for start date
  addStartEvent(type: string, event: MatDatepickerInputEvent<Date>,cartid: string) {
    if( type == "change")
    {
      this.startDate.setDate(event.value.getDate())
      this.startDate.setMonth(event.value.getMonth());
      this.endDate.setDate(event.value.getDate()+1);
      this.endDate.setMonth(event.value.getMonth());
      var startDateString = this.startDate.toISOString().slice(0, 10);
      
      var uid = sessionStorage.getItem('uid')
      if(uid!='null' && uid != undefined){
      this.cartService.updateCart({uid: uid, cartid:cartid,fromDate:startDateString}).subscribe(data =>{
        
        if(!data.success){
          //this.ngOnInit();
          this._snackBar.open(data.message, "ok", {
            duration: 2000,
          });
        } 
      })
    }
    }
    
  }

  // updates user selection for end date

  addEndEvent(type: string, event: MatDatepickerInputEvent<Date>, cartid: string) {
    if( type == "change")
    {
     
      this.endDate.setDate(event.value.getDate());
      this.endDate.setMonth(event.value.getMonth());
    
      var endDateString = this.endDate.toISOString().slice(0, 10);
      var uid = sessionStorage.getItem('uid')
      if(uid!='null' && uid != undefined){
      this.cartService.updateCart({uid: uid, cartid:cartid, toDate: endDateString}).subscribe(data =>{
       

        if(!data.success){
          //this.ngOnInit();
          this._snackBar.open(data.message, "ok", {
            duration: 2000,
          });
        }
      });
    }
    }
    
  }



}
export interface Quantity {
  value: number;
  viewValue: number;

}

@Component({
  selector: 'addToCartDialog',
  templateUrl: 'addToCartDetailsDailog.html',
})
export class AddToCartDialog {
  toppings = new FormControl();
  toppingList: string[] = ['Office 365', 'Adobe Photoshop', 'Visual Studio', 'Android Studio', 'FIFA 19', 'Call of Duty'];
  quantities = new FormControl();
  quantityList: any[] =[]
  startDateString = ""
  endDateString = ""
  startDate = new Date();
  endDate = new Date();
  startDateModel;
  endDateModel;
  cartError:boolean = false;
  startDateError:boolean = false;
  endDateError:boolean = false;
  @Output() cartItem: EventEmitter<any> = new EventEmitter();

  constructor(private cartService:CartService,
    public dialogRef: MatDialogRef<AddToCartDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      var j=0
      for(var i=1;i<=parseInt(data["maxQuantity"]);i++){
        this.quantityList[j]=i
        j++
      }
    }

  onNoClick(): void {
    this.dialogRef.close();
    this.startDateString = ""
  this.endDateString = ""
  }

  //get start date selection from user input
 addStartEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if( type == "change")
    {
      this.startDate.setDate(event.value.getDate())
      this.startDate.setMonth(event.value.getMonth());
      this.endDate.setDate(event.value.getDate()+1);
      this.endDate.setMonth(event.value.getMonth());
       this.startDateString = this.startDate.toISOString().slice(0, 10);
      
     
    }
    
  }

  //get end date selection from user input
  addEndEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if( type == "change")
    {
     
      this.endDate.setDate(event.value.getDate());
      this.endDate.setMonth(event.value.getMonth());
    
       this.endDateString = this.endDate.toISOString().slice(0, 10);

    }
    
  }

  // add item to cart service
  addToCart()
  {

    this.cartError =false;
    this.startDateError=false;
    this.endDateError=false;
    if(!(this.startDateModel && this.startDateModel.value != ''))
    {
    this.startDateError=true;
    }
    if( !(this.endDateModel && this.endDateModel.value != ''))
    {
       this.endDateError=true;
    }
    if( !(this.quantities && this.quantities.value != ''))
    {
      this.cartError=true;
    }

    if( this.cartError == false &&   this.startDateError ==false && this.endDateError == false)
    {
     
      // to be replaced by fromDate and toDate
      var startDateString = this.startDate.toISOString().slice(0, 19).replace('T', ' ');
      var endDateString = this.endDate.toISOString().slice(0, 19).replace('T', ' ');
      
      let c = this.toppings.value + '';
      let  softwares = c.split(",");
      if(this.toppings.value == undefined ){
        softwares[0] = "Default"
      }
      
      var uid = sessionStorage.getItem('uid')
      
      // flow for guest user -> cart items are added to sessionStorage
      if(uid == 'null' || uid == undefined){
        var obj = {

          "Product":this.data["Product"],
          "Company":this.data["Company"],
          "perDay":this.data["perDay"],
          "prodURL": this.data["productImg"],
          "maxQuantity" :this.quantityList,
          "uid" : uid,
          "productID": this.data["prodID"],
          "quantity" : this.quantities.value,
          "softwares":softwares,
          "fromDate": this.startDate,
          "toDate" : this.endDate
        }

        sessionStorage.setItem('cartItem'+ this.data["prodID"],JSON.stringify(obj));
        var cartCount = parseInt(sessionStorage.getItem('cartCount'));

        cartCount = cartCount + 1
        localStorage.setItem('cartCount',''+cartCount);
        this.cartItem.emit(cartCount);

        this.dialogRef.close();

      }else{
        // flow for loggedin user. adding item to cart for user service
        this.cartService.addToCart({
          "uid" : uid,
          "productID": this.data["prodID"],
          "quantity" : this.quantities.value,
          "softwares":softwares,
          "fromDate": startDateString,
          "toDate" : endDateString,
          "maxQuantity" : this.quantityList,
          }).subscribe(data=>{
           
            if(data.success){
              this.cartService.getCartCount({'uid':uid}).subscribe(data=>{
                if(data.success){
                  this.cartItem.emit(data.count);
                }else{
                  this.cartItem.emit(0);
                }
              });
            }
            this.dialogRef.close();
  
          });
  
      }

      
    }
  
  }

}