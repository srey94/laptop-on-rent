/*
Authors : Sreyas Naaraayanan
*/
var sql = require('../../db.js');
var path = require('path');
var bodyParser = require('body-parser');
var CryptoJS = require("crypto-js");

var UserDetails = function (user) {

    this.username = user.username;
    this.otp = user.otp;
    this.password = user.password;
    this.confirmPassword = user.confirmPassword;
}

UserDetails.updatePassword = function (user, res) {


    sql.query("Select uId,email, pswd from authentication where email = ? ", user.username, function (err, result) {


        if (err) {
            console.log("error: ", err);
            res(err, null);
        }
        else {
            var OTP = user.otp;
            var newPassword = user.password;
           

            
            userPassword = result[0].pswd;
            

            var bytes = CryptoJS.AES.decrypt(userPassword, 'cipher1234');
            var originalPassword = bytes.toString(CryptoJS.enc.Utf8);
            

            if (originalPassword == OTP) {
                
                
                var ciphertext = CryptoJS.AES.encrypt(newPassword, 'cipher1234').toString();
                
                var q2 = "update authentication set pswd = '" + ciphertext + "' where email = '" + user.username + "'";
                sql.query(q2, function (e, r) {
                    if (e) {
                        console.log("error :", e);
                        res(e, null);
                    } else {
                        console.log("new password updated in Auth Table")
                    }
                })
                res(null, result);
            }

            else  {
                console.log("generated password mismatch " +user.otp );
                res(null, null);

            }
        }
    });


}

module.exports = UserDetails;