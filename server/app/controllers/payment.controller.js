/*
    Author: Srikrishna Sasidharan
*/

let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');


var PaymentModel = require('../models/payment.model')

// add new payment method for user
exports.addPayment = (req,res)=>{

    class Payment{
        constructor(){
            this.uid = req.body.uid
            this.paymentid = req.body.paymentid
            this.cardNumber = req.body.cardNumber
            this.name = req.body.name
            this.expMonth = req.body.expMonth
            this.expYear = req.body.expYear
            this.type = req.body.type
        }
    }
    var payment = new Payment();
    PaymentModel.addNewPayment(payment, function(error, result){
        if(result){
            res.json({
                success: true,
                message: "Payment added."
            })
        }else{
            res.json({
                success: true,
                message: "Payment not added."
            })
        }
    });

    


}

// get all payment methods for user
exports.getPaymentForUser = (req,res) =>{
    class Payment{
        constructor(){
            this.uid = req.body.uid
            this.paymentid = req.body.paymentid
            this.cardNumber = req.body.cardNumber
            this.name = req.body.name
            this.expMonth = req.body.expMonth
            this.expYear = req.body.expYear
            this.type = req.body.type
        }
    }
    var payment = new Payment();
    PaymentModel.getPaymentForUser(payment, function(error,result){
        var paymentItems=[]
        for (var i = 0; i < result.length; i++){
            paymentItems[i] = result[i]
        }
        if(result){
            res.json({
                success: true,
                payments: paymentItems
              });
        }else{
            res.json({
                success: false,
                message: 'Error while retrieving payment details.'
              });
        }
    })
}